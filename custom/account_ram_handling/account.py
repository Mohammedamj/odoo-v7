# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp.tools.translate import _


    
class account_voucher(osv.osv):
    
    _inherit = 'account.voucher'
 
    def action_move_line_create(self, cr, uid, ids, context=None):
        super(account_voucher,self).action_move_line_create(cr, uid, ids, context)
        account_tax20_achats_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 20% ACHATS')], context=context)
        account_tax14_achats_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 17% ACHATS')], context=context)
        account_tax10_achats_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 10% ACHATS')], context=context)
        account_tax7_achats_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 7% ACHATS')], context=context)
        account_tax20_ventes_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 20% VENTES')], context=context)
        account_tax14_ventes_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 14% VENTES')], context=context)
        account_tax10_ventes_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 10% VENTES')], context=context)
        account_tax7_ventes_decl_id = self.pool.get('account.tax').search(cr, uid, [('name','=','TVA 7% VENTES')], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax20_ventes_decl_id, context=context):
            compte_client_credit_tva_20 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
            compte_client_debit_tva_20 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax14_ventes_decl_id, context=context):
            compte_client_credit_tva_14 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
            compte_client_debit_tva_14 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax10_ventes_decl_id, context=context):
            compte_client_credit_tva_10 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
            compte_client_debit_tva_10 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax7_ventes_decl_id, context=context):
            compte_client_credit_tva_07 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
            compte_client_debit_tva_07 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax20_achats_decl_id, context=context):
            compte_fournisseur_credit_tva_20 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
            compte_fournisseur_debit_tva_20 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax14_achats_decl_id, context=context):
            compte_fournisseur_credit_tva_14 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
            compte_fournisseur_debit_tva_14 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax10_achats_decl_id, context=context):
            compte_fournisseur_credit_tva_10 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
            compte_fournisseur_debit_tva_10 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
        for l in self.pool.get('account.tax').browse(cr, uid, account_tax7_achats_decl_id, context=context):
            compte_fournisseur_credit_tva_07 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_collected_id.code))], context=context)
            compte_fournisseur_debit_tva_07 = self.pool.get('account.account').search(cr, uid, [('code','=',int(l.account_tax_declare.code))], context=context)
        
        
        tva_client_20 = "TVA 20% VENTES"
        tva_client_14 = "TVA 14% VENTES"
        tva_client_10 = "TVA 10% VENTES"
        tva_client_07 = "TVA 7% VENTES"
        tva_fournisseur_20 = "TVA 20% ACHATS"
        tva_fournisseur_14 = "TVA 14% ACHATS"
        tva_fournisseur_10 = "TVA 10% ACHATS"
        tva_fournisseur_07 = "TVA 7% ACHATS"
        
        for record in self.browse(cr, uid, ids, context=context):
            for line in record.voucher_tax_line:
                for tax in record.move_id.line_id:
                    if tax.debit and not tax.credit:
                        if line.name == tva_client_20:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_debit_tva_20[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_client_14:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_debit_tva_14[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_client_10:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_debit_tva_10[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_client_07:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_debit_tva_07[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_20:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_debit_tva_20[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_14:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_debit_tva_14[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_10:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_debit_tva_10[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_07:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_debit_tva_07[0],'reconcile_id': None,'debit':line.tax_amount}, context=context)
                    if tax.credit and not tax.debit:
                        if line.name == tva_client_20:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_credit_tva_20[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_client_14:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_credit_tva_14[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_client_10:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_credit_tva_10[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_client_07:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_client_credit_tva_07[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_20:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_credit_tva_20[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_14:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_credit_tva_14[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_10:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_credit_tva_10[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
                        if line.name == tva_fournisseur_07:
                            self.pool.get('account.move.line').copy(cr, uid, tax.id, default={'account_id': compte_fournisseur_credit_tva_07[0],'reconcile_id': None,'credit':line.tax_amount}, context=context)
        
        return True
    
    _columns = {
        'voucher_tax_line' :  fields.many2many('account.invoice.tax', 'voucher_tax_line_account_voucher_rel', 'voucher_tax_line_id', 'account_voucher_id', 'Lignes de taxes')
    }
    

class account_invoice(osv.osv):
    
    _inherit = 'account.invoice'
    
    def invoice_pay_customer(self, cr, uid, ids, context=None):
        dict = super(account_invoice,self).invoice_pay_customer(cr, uid, ids, context)
        inv = self.browse(cr, uid, ids[0], context=context)
        dict.get('context').update({'default_voucher_tax_line' : [(6, 0, [line_tax.id for line_tax in inv.tax_line])]})
        return dict
    


class account_tax(osv.osv):
     
    _inherit = 'account.tax'
    
    _columns = {
        
        'account_tax_declare' : fields.many2one('account.account', 'Compte de taxe à déclarer')
    
    }