# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
    

class ramh_accomodation_voucher_beneficiaire(osv.osv):
    _name = "ramh.accomodation.voucher.beneficiaire"
    _columns = {
        'beneficiaire': fields.char(''),
        'num_billets': fields.char(''),
        'agence': fields.char('Agence'),
        'lieu': fields.char('Lieu'),
        'date': fields.date('Date'),
        'ramh_bon_beneficiaire_id': fields.many2one('ramh.accomodation.voucher', u'Bénéficiaire')
        }

    
class ramh_accomodation_voucher(osv.osv):
    _name = "ramh.accomodation.voucher"
    _columns = {
        'numero': fields.char(u'Numéro'),
        'passagers': fields.boolean('Passagers'),
        'equipages': fields.boolean('Equipages'),
        'beneficiaire_ids': fields.one2many('ramh.accomodation.voucher.beneficiaire', 'ramh_bon_beneficiaire_id','Bénéficiaires'),
        'correspondance': fields.boolean('Correspondance'),
        'irregularite': fields.boolean(u'Irrégularité'),
        'autres': fields.boolean('Autres'),
        'hotel': fields.char('HOTEL'),
        'date_entree_hotel': fields.date(u'Entrée'),
        'date_sortie_hotel': fields.date('Sortie'),
        'arrivee_de': fields.char(u'Arrivée De'),
        'arrivee_cie': fields.char(u'Arrivée Cie'),
        'arrivee_num_vol': fields.char(u'Arrivée Num Vol'),
        'arrivee_date': fields.date(u'Arrivée Date'),
        'arrivee_heure': fields.char(u'Arrivée Heure'),
        'depart_de': fields.char(u'Départ De'),
        'depart_cie': fields.char(u'Départ Cie'),
        'depart_num_vol': fields.char(u'Départ Num Vol'),
        'depart_date': fields.date(u'Départ Date'),
        'depart_heure': fields.char(u'Départ Heure'),
        'services_ramh_chambre_single': fields.boolean('Chambre Single'),
        'services_ramh_chambre_double': fields.boolean('Chambre Double'),
        'services_ramh_chambre_triple': fields.boolean('Chambre Triple'),
        'services_ramh_day_use_single': fields.boolean('Day Use Single'),
        'services_ramh_day_use_double': fields.boolean('Day Use Double'),
        'services_ramh_day_use_triple': fields.boolean('Day Use Triple'),
        'repas_petit_dejeuner': fields.boolean(u'Petit Déjeuner'),
        'repas_dejeuner': fields.boolean(u'Déjeuner'),
        'repas_diner': fields.boolean('Diner'),
        'repas_rafraichissements': fields.boolean(u'Rafraîchissements'),
        'repas_petit_dejeuner_dt': fields.char(''),
        'repas_dejeuner_dt': fields.char(''),
        'repas_diner_dt': fields.char(''),
        'repas_rafraichissements_dt': fields.char(''),
        'transport_aller': fields.char('ALLER'),
        'transport_aller_ram_h': fields.char('RAM H'),
        'transport_aller_taxi': fields.char('TAXI'),
        'transport_aller_train': fields.char('TRAIN'),
        'transport_aller_bus': fields.char('BUS'),
        'transport_retour': fields.char('RETOUR'),
        'transport_retour_ram_h': fields.char('RAM H'),
        'transport_retour_taxi': fields.char('TAXI'),
        'transport_retour_train': fields.char('TRAIN'),
        'transport_retour_bus': fields.char('BUS'),
        'repartition_frais_ram_h': fields.float('RAM Handling'),
        'repartition_frais_autres_cies': fields.float('AUTRES CIES'),
        'facture_a_adresser_ramh': fields.char('FACTURE A ADRESSER RAM Handling'),
        'bon_emis_par': fields.char('BON EMIS PAR'),
        'ramh_bon_hebergement_id': fields.many2one('ramh.bon.hebergement', 'Bon hebergement')
        
    }

class ramh_bon_hebergement(osv.osv):
    _name = "ramh.bon.hebergement"
    _columns = {
        'ramh_bon_hebergement_ids': fields.one2many('ramh.accomodation.voucher', 'ramh_bon_hebergement_id','BON HEBERGEMENT ACCOMODATION VOUCHER')
    }

