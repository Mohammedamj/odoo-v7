# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp.tools.translate import _


    
class account_voucher(osv.osv):
    
    _inherit = 'account.voucher'
 
    def action_move_line_create(self, cr, uid, ids, context=None):
        super(account_voucher,self).action_move_line_create(cr, uid, ids, context)
        compte_credit = self.pool.get('account.account').search(cr, uid, [('code','=',345800)], context=context)
        compte_debit = self.pool.get('account.account').search(cr, uid, [('code','=',3455220)], context=context)
        for record in self.browse(cr, uid, ids, context=context):
            for line in record.move_id.line_id:
                if line.debit and not line.credit:
                    self.pool.get('account.move.line').copy(cr, uid, line.id, default={'account_id': compte_debit[0],'reconcile_id': None,'debit':record.amount_tax}, context=context)
                    print record
                if line.credit and not line.debit:
                    self.pool.get('account.move.line').copy(cr, uid, line.id, default={'account_id': compte_credit[0],'reconcile_id': None,'credit':record.amount_tax}, context=context)
        
        return True

    
    _columns = {
        'voucher_tax_line' : fields.many2many('account.invoice.tax' , 'Lignes de taxes', readonly=True, states={'draft':[('readonly',False)]})
    }
    

class account_invoice(osv.osv):
    
    _inherit = 'account.invoice'
    
    def invoice_pay_customer(self, cr, uid, ids, context=None):
        dict = super(account_invoice,self).invoice_pay_customer(cr, uid, ids, context)
        inv = self.browse(cr, uid, ids[0], context=context)
        dict.get('context').update({'default_voucher_tax_line' : [(6, 0, [line_tax.id for line_tax in inv.tax_line])]})
        return dict
    
        
