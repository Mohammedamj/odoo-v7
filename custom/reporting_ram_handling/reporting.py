# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp.tools.translate import _
import base64
    



class donnees_clients_commercial_ramh_reporting(osv.osv):
    _name = "donnees.clients.commercial.ramh.reporting"
    _columns = {
        'code_client': fields.char('Code du client'),
        'nom_client': fields.char('Nom du client'),
        'ca_annuel_estime': fields.float('CA annuel estimé'),
        'probabilite_perte': fields.float(u'Probabilité de perte'),
        'evenement': fields.char(u'Evénement'),
        'impact_ca': fields.float('Impact sur le CA'),
        'donnees_clients_id': fields.many2one('commercial', u'données clients')
        }

class produits_exploitation_ramh_reporting(osv.osv):
    _name = "produits.exploitation.ramh.reporting"
    _columns = {
        'produit_exploitation': fields.char(u'Produit d’exploitation'),
        'montant':fields.float('Montant'),
        'produits_exploitation_id': fields.many2one('commercial', 'produits exploitation')
        }

class faits_marquants_commercial_ramh_reporting(osv.osv):
    _name = "faits.marquants.commercial.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_commercial_id': fields.many2one('commercial', 'faits marquants')
        }

class faits_marquants_finance_ramh_reporting(osv.osv):
    _name = "faits.marquants.finance.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_finance_id': fields.many2one('finance', 'faits marquants')
        }
    
class faits_marquants_ressources_humaines_ramh_reporting(osv.osv):
    _name = "faits.marquants.ressources.humaines.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_ressources_humaines_id': fields.many2one('ressources.humaines', 'faits marquants')
        }

class faits_marquants_qualite_ramh_reporting(osv.osv):
    _name = "faits.marquants.qualite.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_qualite_id': fields.many2one('qualite', 'faits marquants')
        }

class faits_marquants_system_information_ramh_reporting(osv.osv):
    _name = "faits.marquants.system.information.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_system_information_id': fields.many2one('system.information', 'faits marquants')
        }

class faits_marquants_passage_ramh_reporting(osv.osv):
    _name = "faits.marquants.passage.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_passage_id': fields.many2one('passage', 'faits marquants')
        }

class faits_marquants_ramp_ramh_reporting(osv.osv):
    _name = "faits.marquants.ramp.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_ramp_id': fields.many2one('ramp', 'faits marquants')
        }

class faits_marquants_supervision_ramp_ramh_reporting(osv.osv):
    _name = "faits.marquants.supervision.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_supervision_id': fields.many2one('supervision', 'faits marquants')
        }

class faits_marquants_gse_ramh_reporting(osv.osv):
    _name = "faits.marquants.gse.ramh.reporting"
    _columns = {
        'numero': fields.char('Numéro'),
        'text':fields.text('Faits marquants'),
        'faits_marquants_gse_id': fields.many2one('gse', 'faits marquants')
        }

class chiffres_affaires_ramh_reporting(osv.osv):
    _name = "chiffres.affaires.ramh.reporting"
    _columns = {
        'code_client': fields.char('Code du client'),
        'nom_client': fields.char('Nom du client'),
        'touchees_ponderees':fields.float(u'Touchées pondérées '),
        'forfait':fields.float('Forfait'),
        'extra':fields.float('Extra'),
        'chiffres_affaires_id': fields.many2one('commercial', 'faits marquants')
        }

class commercial(osv.osv):
    _name = "commercial"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.commercial.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_commercial_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_donnees_clients(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('donnees.clients.commercial.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_donnees_clients']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["code_client"] = line[0]
                data["nom_client"] = line[1]
                data["ca_annuel_estime"] = line[2]
                data["probabilite_perte"] = line[3]
                data["evenement"] = line[4]
                data["impact_ca"] = line[5]
                data["donnees_clients_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_chiffre_affaires(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('chiffres.affaires.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_chiffre_affaires']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["code_client"] = line[0]
                data["nom_client"] = line[1]
                data["touchees_ponderees"] = line[2]
                data["forfait"] = line[3]
                data["extra"] = line[4]
                data["chiffres_affaires_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_produits_exploitation(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('produits.exploitation.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_produits_exploitation']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["produit_exploitation"] = line[0]
                data["montant"] = line[1]
                data["produits_exploitation_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', u'Périod'),
        'faits_marquants_commercial_ids': fields.one2many('faits.marquants.commercial.ramh.reporting', 'faits_marquants_commercial_id','Faits marquants'),
        'donnees_clients_line_ids': fields.one2many('donnees.clients.commercial.ramh.reporting', 'donnees_clients_id', u'données clients'),
        'chiffre_affaires_ids': fields.one2many('chiffres.affaires.ramh.reporting', 'chiffres_affaires_id', u'Chiffres d’affaires'),
        'produits_exploitation_line_ids': fields.one2many('produits.exploitation.ramh.reporting', 'produits_exploitation_id', u'Autres produits d’exploitation'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_donnees_clients': fields.binary('Importer CSV'),
        'binary_csv_chiffre_affaires': fields.binary('Importer CSV'),
        'binary_csv_produits_exploitation': fields.binary('Importer CSV')
    }



class charges_exploitation_ramh_reporting(osv.osv):
    _name = "charges.exploitation.ramh.reporting"
    _columns = {
        'total': fields.float('Total')
    }

class cout_du_personnel_ramh_reporting(osv.osv):
    _name = "cout.du.personnel.ramh.reporting"
    _columns = {
        'masse_salariale': fields.float('Masse salariale'),
        'transport_personnel_aeroport_domicile': fields.float(u'Transport Personnel Aéroport – Domicile'),
        'habillement': fields.float('Habillement'),
        'formation': fields.float('Formation'),
        'autres_frais_personnel': fields.float('Autres frais du personnel'),
        'total': fields.float('Total'),
        'cout_personnel_id': fields.many2one('finance', 'cout du personnel')
    }

class sous_traitance_ramh_reporting(osv.osv):
    _name = "sous.traitance.ramh.reporting"
    _columns = {
        'manutention': fields.float('Manutention'),
        'mise_disposition': fields.float(u'Mise à disposition'),
        'ATLAS_CATERING': fields.float('ATLAS_CATERING'),
        'SECURITE': fields.float('SECURITE'),
        'NETTOYAGE_LOCAUX': fields.float('NETTOYAGE LOCAUX'),
        'direction_operations': fields.float('Direction Opérations'),
        'total': fields.float('Total'),
        'sous_traitance_id': fields.many2one('finance', 'Sous traitance')
    }
    
class couts_gse_ramh_reporting(osv.osv):
    _name = "couts.gse.ramh.reporting"
    _columns = {
        'frais_entretien': fields.float('Frais entretien'),
        'location_materiel_GSE': fields.float(u'Location matériel GSE'),
        'transport_GSE': fields.float('Transport GSE'),
        'redevances_leaseback': fields.float('Redevances leaseback'),
        'provision_dep_stock_reforme': fields.float(u'Provision dép. stock réformé'),
        'total': fields.float('Total'),
        'couts_gse_id': fields.many2one('finance', 'Couts GSE')
    }

class couts_externes_ramh_reporting(osv.osv):
    _name = "couts.externes.ramh.reporting"
    _columns = {
        'carburant': fields.float('Carburant'),
        'transport_equipage_piste': fields.float(u'Transport équipage piste'),
        'transport_personnel_piste': fields.float('Transport Personnel Piste'),
        'navettes_business_class': fields.float('Navettes Business Class'),
        'imprimes': fields.float(u'Imprimés'),
        'OM': fields.float('OM'),
        'communications': fields.float('Communications'),
        'telephone': fields.float(u'Téléphone'),
        'locations_voitures': fields.float('Locations voitures'),
        'vip_salon': fields.float('VIP Salon'),
        'plastification_bagages': fields.float('Plastification bagages'),
        'irregularites': fields.float(u'Irrégularités'),
        'consultations': fields.float('Consultations'),
        'total': fields.float('Total'),
        'couts_externes_id': fields.many2one('finance', 'Couts externes')
    }

class frais_fonctionnement_ramh_reporting(osv.osv):
    
    _name = "frais.fonctionnement.ramh.reporting"
    
    _columns = {
        'assurances': fields.float('Assurances'),
        'Honoraires': fields.float('Honoraires'),
        'prestations_SI': fields.float('Prestations SI'),
        'services_administratifs_support': fields.float('Services administratifs & Support'),
        'autres_FG': fields.float('Autres FG'),
        'frais_commissions_bancaires': fields.float('Frais et commissions bancaires'),
        'cargo_Emirates': fields.float('Cargo Emirates'),
        'total': fields.float('Total'),
        'frais_fonctionnement_id': fields.many2one('finance', 'Frais fonctionnement')
    }


class couts_infrastructure_ramh_reporting(osv.osv):
    _name = "couts.infrastructure.ramh.reporting"
    _columns = {
        'loyers_ONDA': fields.float('Loyers ONDA'),
        'eau_gaz_electricite': fields.float(u'Eau, Gaz, Electricité'),
        'royalties_ONDA_compagnies_assistees': fields.float('Royalties ONDA Compagnies Assistées'),
        'royalties_ONDA_RAM': fields.float('Royalties ONDA RAM'),
        'impots_taxes': fields.float('Impôts et taxes'),
        'provision_loyer': fields.float('Provision Loyer'),
        'total': fields.float('Total'),
        'couts_infrastructure_id': fields.many2one('finance', 'Couts infrastructure')
    }

class amortissements_ramh_reporting(osv.osv):
    _name = "amortissements.ramh.reporting"
    _columns = {
        'GSE': fields.float('GSE'),
        'Autres': fields.float('Autres'),
        'total': fields.float('Total'),
        'amortissements_id': fields.many2one('finance', 'Amortissements')
    }

class finance(osv.osv):
    _name = "finance"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.finance.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_finance_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_cout_personnel(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('cout.du.personnel.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_cout_du_personnel']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["masse_salariale"] = line[0]
                data["transport_personnel_aeroport_domicile"] = line[1]
                data["habillement"] = line[2]
                data["formation"] = line[3]
                data["autres_frais_personnel"] = line[4]
                data["total"] = line[5]
                data["cout_personnel_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_sous_traitance(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('sous.traitance.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_sous_traitance']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["manutention"] = line[0]
                data["mise_disposition"] = line[1]
                data["ATLAS_CATERING"] = line[2]
                data["SECURITE"] = line[3]
                data["NETTOYAGE_LOCAUX"] = line[4]
                data["direction_operations"] = line[5]
                data["total"] = line[6]
                data["sous_traitance_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_couts_gse(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('couts.gse.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_couts_gse']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["frais_entretien"] = line[0]
                data["location_materiel_GSE"] = line[1]
                data["transport_GSE"] = line[2]
                data["redevances_leaseback"] = line[3]
                data["provision_dep_stock_reforme"] = line[4]
                data["total"] = line[5]
                data["couts_gse_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_couts_externes(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('couts.externes.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_couts_externes']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["carburant"] = line[0]
                data["transport_equipage_piste"] = line[1]
                data["transport_personnel_piste"] = line[2]
                data["navettes_business_class"] = line[3]
                data["imprimes"] = line[4]
                data["OM"] = line[5]
                data["communications"] = line[6]
                data["telephone"] = line[7]
                data["locations_voitures"] = line[8]
                data["vip_salon"] = line[9]
                data["plastification_bagages"] = line[10]
                data["irregularites"] = line[11]
                data["consultations"] = line[12]
                data["total"] = line[13]
                data["couts_externes_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_frais_fonctionnement(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('frais.fonctionnement.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_frais_fonctionnement']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["assurances"] = line[0]
                data["Honoraires"] = line[1]
                data["prestations_SI"] = line[2]
                data["services_administratifs_support"] = line[3]
                data["autres_FG"] = line[4]
                data["frais_commissions_bancaires"] = line[5]
                data["cargo_Emirates"] = line[6]
                data["total"] = line[7]
                data["frais_fonctionnement_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_couts_infrastructure(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('couts.infrastructure.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_couts_infrastructure']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["loyers_ONDA"] = line[0]
                data["eau_gaz_electricite"] = line[1]
                data["royalties_ONDA_compagnies_assistees"] = line[2]
                data["royalties_ONDA_RAM"] = line[3]
                data["impots_taxes"] = line[4]
                data["provision_loyer"] = line[5]
                data["total"] = line[6]
                data["couts_infrastructure_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_amortissements(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('amortissements.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_amortissements']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["GSE"] = line[0]
                data["Autres"] = line[1]
                data["total"] = line[2]
                data["amortissements_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_finance_ids': fields.one2many('faits.marquants.finance.ramh.reporting', 'faits_marquants_finance_id','Faits marquants'),
        'charges_exploitation_id': fields.many2one('charges.exploitation.ramh.reporting', 'charges exploitation'),
        'cout_du_personnel_ids': fields.one2many('cout.du.personnel.ramh.reporting', 'cout_personnel_id', u'Coût du personnel'),
        'sous_traitance_ids': fields.one2many('sous.traitance.ramh.reporting', 'sous_traitance_id', 'Sous-traitance'),
        'couts_gse_ids': fields.one2many('couts.gse.ramh.reporting', 'couts_gse_id', u'Coûts GSE'),
        'couts_externes_ids': fields.one2many('couts.externes.ramh.reporting', 'couts_externes_id', u'Coûts externes'),
        'frais_fonctionnement_ids': fields.one2many('frais.fonctionnement.ramh.reporting', 'frais_fonctionnement_id', 'Frais de fonctionnement'),
        'couts_infrastructure_ids': fields.one2many('couts.infrastructure.ramh.reporting', 'couts_infrastructure_id', u'Coûts infrastructure'),
        'amortissements_ids': fields.one2many('amortissements.ramh.reporting', 'amortissements_id', 'Amortissements'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_charges_exploitation': fields.binary('Importer CSV'),
        'binary_csv_cout_du_personnel': fields.binary('Importer CSV'),
        'binary_csv_sous_traitance': fields.binary('Importer CSV'),
        'binary_csv_couts_gse': fields.binary('Importer CSV'),
        'binary_csv_couts_externes': fields.binary('Importer CSV'),
        'binary_csv_frais_fonctionnement': fields.binary('Importer CSV'),
        'binary_csv_couts_infrastructure': fields.binary('Importer CSV'),
        'binary_csv_amortissements': fields.binary('Importer CSV')
        }



class effectif_exploitation_ramh_reporting(osv.osv):
    _name = "effectif.exploitation.ramh.reporting" 
    _columns = {
        'effectif_exploi_ids': fields.one2many('effectif.exploitation.option.ramh.reporting', 'effectif_exploitation_option_id', 'effectif exploitation'),
        'effectif': fields.float('Effectif'),
        'masse_salariale': fields.float('Masse salariale'),
        'total': fields.float('TOTAL EFFECTIF EXPLOITATION'),
        'effectif_exploitation_id': fields.many2one('ressources.humaines', 'Effectif Exploitation')
        }

class effectif_exploitation_option_ramh_reporting(osv.osv):
    _name = "effectif.exploitation.option.ramh.reporting"
    _columns = {
        'passage_id': fields.many2one('hr.job', 'Passage'),
        'RAMP_id': fields.many2one('hr.job', 'RAMP'),
        'supervision_id': fields.many2one('hr.job', 'Supervision'),
        'gse_id': fields.many2one('hr.job', 'GSE'),
        'effectif_exploitation_option_id': fields.many2one('effectif.exploitation.ramh.reporting', 'EFFECTIF EXPLOITATION')
        }

class effectif_administratif_ramh_reporting(osv.osv):
    _name = "effectif.administratif.ramh.reporting" 
    _columns = {
        'effectif_admin_ids': fields.many2one('hr.job', 'EFFECTIF ADMINISTRATIF '),
        'effectif': fields.float('Effectif'),
        'masse_salariale': fields.float('Masse salariale'),
        'total': fields.float('TOTAL EFFECTIF ADMINISTRATIF'),
        'effectif_administratif_id': fields.many2one('ressources.humaines', 'Effectif Exploitation')
        }

class mise_disposition_ramh_reporting(osv.osv):
    _name = "mise.disposition.ramh.reporting" 
    _columns = {
        'mise_dispo_ids': fields.many2one('hr.job', 'MISE A DISPOSITION'),
        'effectif': fields.float('Effectif'),
        'masse_salariale': fields.float('Masse salariale'),
        'total': fields.float('TOTAL MISE A DISPOSITION '),
        'disposition_id': fields.many2one('ressources.humaines', 'mise a disposition')
        }

class autres_indicateurs_rh_absences_ramh_reporting(osv.osv):
    _name = "autres.indicateurs.rh.absences.ramh.reporting"
    _columns = {
        'fonction': fields.char('Fonction'),
        'nombre_absences': fields.float('Nombre d’absences'),
        'Duree_absences_justifies': fields.float(u'Durée des absences justifiées'),
        'Duree_absences_injustifiees': fields.float(u'Durée des absences injustifiées '),
        'Nombre_total_jours_absences_injustifiees': fields.float(u'Nombre total des jours d’absences injustifiées'),
        'autre_indicateurs_rh_absences_id': fields.many2one('ressources.humaines', 'autre indicateurs')
        }

class autres_indicateurs_rh_engagement_ramh_reporting(osv.osv):
    _name = "autres.indicateurs.rh.engagement.ramh.reporting"
    _columns = {
        'fonction': fields.char('Fonction'),
        'recrutements': fields.float('Recrutements'),
        'licenciements': fields.float('Licenciements'),
        'demissions': fields.float(u'Démissions'),
        'fin_engagement': fields.float(u'Fin d’engagement'),
        'autre_indicateurs_rh_engagement_id': fields.many2one('ressources.humaines', 'autre indicateurs')
        }
    
class autres_indicateurs_rh_formation_ramh_reporting(osv.osv):
    _name = "autres.indicateurs.rh.formation.ramh.reporting"
    _columns = {
        'fonction': fields.char('Fonction'),
        'type_formation': fields.char('Type de formation'),
        'nombre_jours_formation': fields.float('Nombre de jours de formation'),
        'nombre_participants': fields.float('Nombre de participants'),
        'autre_indicateurs_rh_formation_id': fields.many2one('ressources.humaines', 'autre indicateurs')
        }

class ressources_humaines(osv.osv):
    _name = "ressources.humaines"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.ressources.humaines.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_ressources_humaines_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autres_indicateurs_rh_absences(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autres.indicateurs.rh.absences.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autres_indicateurs_rh_absences']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["fonction"] = line[0]
                data["nombre_absences"] = line[1]
                data["Duree_absences_justifies"] = line[2]
                data["Duree_absences_injustifiees"] = line[3]
                data["Nombre_total_jours_absences_injustifiees"] = line[4]
                data["autre_indicateurs_rh_absences_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autres_indicateurs_rh_engagement(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autres.indicateurs.rh.engagement.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autres_indicateurs_rh_engagement']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["fonction"] = line[0]
                data["recrutements"] = line[1]
                data["licenciements"] = line[2]
                data["demissions"] = line[3]
                data["fin_engagement"] = line[4]
                data["autre_indicateurs_rh_engagement_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autres_indicateurs_rh_formation(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autres.indicateurs.rh.formation.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autres_indicateurs_rh_formation']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["fonction"] = line[0]
                data["type_formation"] = line[1]
                data["nombre_jours_formation"] = line[2]
                data["nombre_participants"] = line[3]
                data["autre_indicateurs_rh_formation_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_ressources_humaines_ids': fields.one2many('faits.marquants.ressources.humaines.ramh.reporting', 'faits_marquants_ressources_humaines_id','Faits marquants'),
        'effectif_exploitation_ids': fields.one2many('effectif.exploitation.ramh.reporting', 'effectif_exploitation_id', 'EFFECTIF EXPLOITATION'),
        'effectif_administratif_ids': fields.one2many('effectif.administratif.ramh.reporting', 'effectif_administratif_id', 'EFFECTIF ADMINISTRATIF '),
        'mise_disposition_ids': fields.one2many('mise.disposition.ramh.reporting', 'disposition_id', 'MISE A DISPOSITION'),
        'autres_indicateurs_rh_absences_ids': fields.one2many('autres.indicateurs.rh.absences.ramh.reporting', 'autre_indicateurs_rh_absences_id', 'Autres Indicateurs RH Absences'),
        'autres_indicateurs_rh_engagement_ids': fields.one2many('autres.indicateurs.rh.engagement.ramh.reporting', 'autre_indicateurs_rh_engagement_id', 'Autres Indicateurs RH Engagement'),
        'autres_indicateurs_rh_formation_ids': fields.one2many('autres.indicateurs.rh.formation.ramh.reporting', 'autre_indicateurs_rh_formation_id', 'Autres Indicateurs RH Formation'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_autres_indicateurs_rh_absences': fields.binary('Importer CSV'),
        'binary_csv_autres_indicateurs_rh_engagement': fields.binary('Importer CSV'),
        'binary_csv_autres_indicateurs_rh_formation': fields.binary('Importer CSV')
        }


class qualite_indicateur_ramh_reporting(osv.osv):
    _name = "qualite.indicateur.ramh.reporting"
    _columns = {
        'ponctualite_relative': fields.float(u'Ponctualité relative'),
        'ponctualite_absolue': fields.float(u'Ponctualité absolue '),
        'taux_spoliation': fields.float('Taux de spoliation'),
        'taux_bagages_retardes': fields.float(u'Taux de bagages retardés'),
        'taux_bagages_endomages': fields.float(u'Taux de bagages endomagés'),
        'audits_realises': fields.float(u'Audits réalisés '),
        'qualite_indicateur_id': fields.many2one('qualite', 'qualite indicateurs')
        }

class qualite(osv.osv):
    _name = "qualite"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.qualite.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_qualite_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_qualite_indicateur(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('qualite.indicateur.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_qualite_indicateur']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["ponctualite_relative"] = line[0]
                data["ponctualite_absolue"] = line[1]
                data["taux_spoliation"] = line[2]
                data["taux_bagages_retardes"] = line[3]
                data["taux_bagages_endomages"] = line[4]
                data["audits_realises"] = line[5]
                data["qualite_indicateur_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_qualite_ids': fields.one2many('faits.marquants.qualite.ramh.reporting', 'faits_marquants_qualite_id','Faits marquants'),
        'qualite_indicateur_ids': fields.one2many('qualite.indicateur.ramh.reporting', 'qualite_indicateur_id', u'Indicateurs Qualité '),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_qualite_indicateur': fields.binary('Importer CSV')
        }


class system_information(osv.osv):
    _name = "system.information"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.system.information.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_system_information_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_system_information_ids': fields.one2many('faits.marquants.system.information.ramh.reporting', 'faits_marquants_system_information_id','Faits marquants'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV')
        }

class indicateur_ponctualite_ramh_reporting(osv.osv):
    _name = "indicateur.ponctualite.ramh.reporting"
    _columns = {
        'nombre_retards_passage': fields.float('Nombre de retards passage'),
        'taux_ponctualite_passage': fields.float(u'Taux de ponctualité passage '),
        'indicateur_ponctualite_id': fields.many2one('passage', u'Indicateur de ponctualite')
        }

class indicateurs_taux_satisfaction_client_ramh_reporting(osv.osv):
    _name = "indicateurs.taux.satisfaction.client.ramh.reporting"
    _columns = {
        'enregistrement': fields.float('Enregistrement'),
        'embarquement': fields.float('Embarquement'),
        'salon_VIP': fields.float('Salon VIP '),
        'correspondance': fields.float('Correspondance'),
        'indicateurs_satisfaction_id': fields.many2one('passage', 'Indicateur de satisfaction')
        }

class autre_indicateurs_passage_annonce_ramh_reporting(osv.osv):
    _name = "autre.indicateurs.passage.annonce.ramh.reporting"
    _columns = {
        'indicateur': fields.char('Indicateur'),
        'conforme': fields.float('Conforme'),
        'non_conforme': fields.float('Non conforme'),
        'mesure': fields.float('Mesure'),
        'taux_conformite': fields.float(u'Taux de conformité'),
        'autre_indicateurs_passage_annonce_id': fields.many2one('passage', 'Autre indicateur de passage')
        }

class autre_indicateurs_passage_salle_ramh_reporting(osv.osv):
    _name = "autre.indicateurs.passage.salle.ramh.reporting"
    _columns = {
        'indicateur': fields.char('Indicateur'),
        'conforme': fields.float('Conforme'),
        'non_conforme': fields.float('Non conforme'),
        'mesure': fields.float('Mesure'),
        'taux_conformite': fields.float(u'Taux de conformité'),
        'autre_indicateurs_passage_salle_id': fields.many2one('passage', 'Autre indicateur de passage')
        }

class autre_indicateurs_passage_embarquement_ramh_reporting(osv.osv):
    _name = "autre.indicateurs.passage.embarquement.ramh.reporting"
    _columns = {
        'indicateur': fields.char('Indicateur'),
        'conforme': fields.float('Conforme'),
        'non_conforme': fields.float('Non conforme'),
        'mesure': fields.float('Mesure'),
        'taux_conformite': fields.float(u'Taux de conformité'),
        'autre_indicateurs_passage_embarquement_id': fields.many2one('passage', 'Autre indicateur de passage')
        }

class autre_indicateurs_passage_check_in_ramh_reporting(osv.osv):
    _name = "autre.indicateurs.passage.check.in.ramh.reporting"
    _columns = {
        'indicateur': fields.char('Indicateur'),
        'conforme': fields.float('Conforme'),
        'non_conforme': fields.float('Non conforme'),
        'mesure': fields.float('Mesure'),
        'taux_conformite': fields.float(u'Taux de conformité'),
        'autre_indicateurs_passage_check_in_id': fields.many2one('passage', 'Autre indicateur de passage')
        }

class passage(osv.osv):
    _name = "passage"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.passage.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_passage_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_indicateur_ponctualite(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('indicateur.ponctualite.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_indicateur_ponctualite']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["nombre_retards_passage"] = line[0]
                data["taux_ponctualite_passage"] = line[1]
                data["indicateur_ponctualite_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_indicateur_satisfaction_client(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('indicateurs.taux.satisfaction.client.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_indicateur_satisfaction_client']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["enregistrement"] = line[0]
                data["embarquement"] = line[1]
                data["salon_VIP"] = line[2]
                data["correspondance"] = line[3]
                data["indicateurs_satisfaction_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autre_indicateurs_passage_annonce(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autre.indicateurs.passage.annonce.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autre_indicateurs_passage_annonce']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["indicateur"] = line[0]
                data["conforme"] = line[1]
                data["non_conforme"] = line[2]
                data["mesure"] = line[3]
                data["taux_conformite"] = line[4]
                data["autre_indicateurs_passage_annonce_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autre_indicateurs_passage_salle(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autre.indicateurs.passage.salle.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autre_indicateurs_passage_salle']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["indicateur"] = line[0]
                data["conforme"] = line[1]
                data["non_conforme"] = line[2]
                data["mesure"] = line[3]
                data["taux_conformite"] = line[4]
                data["autre_indicateurs_passage_salle_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autre_indicateurs_passage_embarquement(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autre.indicateurs.passage.embarquement.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autre_indicateurs_passage_embarquement']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["indicateur"] = line[0]
                data["conforme"] = line[1]
                data["non_conforme"] = line[2]
                data["mesure"] = line[3]
                data["taux_conformite"] = line[4]
                data["autre_indicateurs_passage_embarquement_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_autre_indicateurs_passage_check_in(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('autre.indicateurs.passage.check.in.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_autre_indicateurs_passage_check_in']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["indicateur"] = line[0]
                data["conforme"] = line[1]
                data["non_conforme"] = line[2]
                data["mesure"] = line[3]
                data["taux_conformite"] = line[4]
                data["autre_indicateurs_passage_check_in_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_passage_ids': fields.one2many('faits.marquants.passage.ramh.reporting', 'faits_marquants_passage_id','Faits marquants'),
        'indicateur_ponctualite_ids': fields.one2many('indicateur.ponctualite.ramh.reporting', 'indicateur_ponctualite_id', u'Indicateur ponctualité'),
        'indicateur_satisfaction_client_ids': fields.one2many('indicateurs.taux.satisfaction.client.ramh.reporting', 'indicateurs_satisfaction_id', 'Indicateurs taux satisfaction client'),
        'autre_indicateurs_passage_annonce_ids': fields.one2many('autre.indicateurs.passage.annonce.ramh.reporting', 'autre_indicateurs_passage_annonce_id', 'Autre indicateurs de passage'),
        'autre_indicateurs_passage_salle_ids': fields.one2many('autre.indicateurs.passage.salle.ramh.reporting', 'autre_indicateurs_passage_salle_id', 'Autre indicateurs de passage'),
        'autre_indicateurs_passage_embarquement_ids': fields.one2many('autre.indicateurs.passage.embarquement.ramh.reporting', 'autre_indicateurs_passage_embarquement_id', 'Autre indicateurs de passage'),
        'autre_indicateurs_passage_check_in_ids': fields.one2many('autre.indicateurs.passage.check.in.ramh.reporting', 'autre_indicateurs_passage_check_in_id', 'Autre indicateurs de passage'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_indicateur_ponctualite': fields.binary('Importer CSV'),
        'binary_csv_indicateur_satisfaction_client': fields.binary('Importer CSV'),
        'binary_csv_autre_indicateurs_passage_annonce': fields.binary('Importer CSV'),
        'binary_csv_autre_indicateurs_passage_salle': fields.binary('Importer CSV'),
        'binary_csv_autre_indicateurs_passage_embarquement': fields.binary('Importer CSV'),
        'binary_csv_autre_indicateurs_passage_check_in': fields.binary('Importer CSV')
        }

class indicateurs_ramp_ramh_reporting(osv.osv):
    _name = "indicateurs.ramp.ramh.reporting"
    _columns = {
        'ponctualite': fields.float(u'Ponctualité'),
        'livraison_bagages': fields.float('Livraison bagages'),
        'ahl': fields.float(u'AHL (bagages retardés)'),
        'dpr': fields.float(u'DPR (bagages endommagés)'),
        'indicateurs_ramp_id': fields.many2one('passage', 'Indicateurs RAMP')
        }

class ramp(osv.osv):
    _name = "ramp"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.ramp.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_ramp_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_indicateurs_ramp(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('indicateurs.ramp.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_indicateurs_ramp']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["ponctualite"] = line[0]
                data["livraison_bagages"] = line[1]
                data["ahl"] = line[2]
                data["dpr"] = line[1]
                data["indicateurs_ramp_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_ramp_ids': fields.one2many('faits.marquants.ramp.ramh.reporting', 'faits_marquants_ramp_id','Faits marquants'),
        'indicateurs_ramp_ids': fields.one2many('indicateurs.ramp.ramh.reporting', 'indicateurs_ramp_id', 'Indicateurs RAMP'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_indicateurs_ramp': fields.binary('Importer CSV')
        }

class indicateurs_supervision_ponctualite_ramh_reporting(osv.osv):
    _name = "indicateurs.supervision.ponctualite.ramh.reporting"
    _columns = {
        'relative_Handling': fields.float('relative Handling'),
        'relative_coordination': fields.float('relative de coordination'),
        'absolue_handling': fields.float('absolue Handling'),
        'absolue_coordination': fields.float('absolue de coordination'),
        'indicateurs_supervision_ponctualite_id': fields.many2one('passage', u'Indicateurs Supervision ponctualité')
        }

class indicateurs_plan_supervision_ramh_reporting(osv.osv):
    _name = "indicateurs.plan.supervision.ramh.reporting"
    _columns = {
        'ecart_documentaire': fields.float('Ecart documentaire'),
        'incident_coordination': fields.float('Incident de coordination'),
        'incidents_reglementaires': fields.float(u'Incidents réglementaires'),
        'indicateurs_plan_supervision_id': fields.many2one('passage', 'Indicateur plan de surveillance')
        }

class supervision(osv.osv):
    _name = "supervision"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.supervision.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_supervision_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_indicateurs_supervision_ponctualite(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('indicateurs.supervision.ponctualite.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_indicateurs_supervision_ponctualite']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["relative_Handling"] = line[0]
                data["relative_coordination"] = line[1]
                data["absolue_handling"] = line[2]
                data["absolue_coordination"] = line[3]
                data["indicateurs_supervision_ponctualite_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_indicateurs_plan_supervision(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('indicateurs.plan.supervision.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_indicateurs_plan_supervision']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["ecart_documentaire"] = line[0]
                data["incident_coordination"] = line[1]
                data["incidents_reglementaires"] = line[2]
                data["indicateurs_plan_supervision_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_supervision_ids': fields.one2many('faits.marquants.supervision.ramh.reporting', 'faits_marquants_supervision_id','Faits marquants'),
        'indicateurs_supervision_ponctualite_ids': fields.one2many('indicateurs.supervision.ponctualite.ramh.reporting', 'indicateurs_supervision_ponctualite_id', 'Indicateurs Supervision Ponctualité'),
        'indicateurs_plan_supervision_ids': fields.one2many('indicateurs.plan.supervision.ramh.reporting', 'indicateurs_plan_supervision_id', 'Indicateur plan de surveillance'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_indicateurs_supervision_ponctualite': fields.binary('Importer CSV'),
        'binary_csv_indicateurs_plan_supervision': fields.binary('Importer CSV')
        }

class indicateurs_gse_ramh_reporting(osv.osv):
    _name = "indicateurs.gse.ramh.reporting"
    _columns = {
        'taux_disponibilite_moyen': fields.float(u'Taux de disponibilité moyen'),
        'nombre_intervention_curative': fields.float(u'Nombre d’intervention curative '),
        'nombre_intervention_preventive': fields.float(u'Nombre d’intervention préventive'),
        'cout_maintenance': fields.float(u'Coût  maintenance'),
        'cout_consommation_carburant': fields.float(u'Coût consommation carburant'),
        'indicateurs_gse_id': fields.many2one('passage', 'Indicateurs GSE')
        }

class gse(osv.osv):
    
    _name = "gse"
    
    def import_csv_fait_marquant(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('faits.marquants.gse.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_faits_marquants']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["numero"] = line[0]
                data["text"] = line[1]
                data["faits_marquants_gse_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    def import_csv_indicateurs_gse(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid,ids, context=context):
            print "Boocle rec"
        declaration_line = self.pool.get('indicateurs.gse.ramh.reporting')
        data_table = self.read(cr, uid, ids[0],  context=context)
        bal_field=data_table['binary_csv_indicateurs_gse']
        if not bal_field:
            raise osv.except_osv(('Erreur!'), ("Veuillez indiquer le fichier CSV !"))
        data_bal=base64.decodestring(bal_field)
        if data_bal:
            reader = data_bal.split('\n')
            data = {}
            i = 0
            for row in reader:
                i += 1
                if i == 1:
                    continue
                line = row.split(';')
                if len(line) == 1:
                    continue
                empty_row = True
                for l in line:
                    if l.strip():
                        empty_row = False
                        break
                if empty_row:
                    continue
                data["taux_disponibilite_moyen"] = line[0]
                data["nombre_intervention_curative"] = line[1]
                data["nombre_intervention_preventive"] = line[2]
                data["cout_maintenance"] = line[3]
                data["cout_consommation_carburant"] = line[4]
                data["indicateurs_gse_id"] = rec.id
                declaration_line.create(cr,uid,data)
                data = {}
        """  
        except Exception:
            raise osv.except_osv(('Erreur!'), ("Impossible d'importer le fichier, veuillez vérifier que le fichier est correct !"))
        """
        return True
    
    _columns = {
        'period_id': fields.many2one('account.period', 'Period'),
        'faits_marquants_gse_ids': fields.one2many('faits.marquants.gse.ramh.reporting', 'faits_marquants_gse_id','Faits marquants'),
        'indicateurs_gse_ids': fields.one2many('indicateurs.gse.ramh.reporting', 'indicateurs_gse_id', 'Indicateurs GSE'),
        'binary_csv_faits_marquants': fields.binary('Importer CSV'),
        'binary_csv_indicateurs_gse': fields.binary('Importer CSV')
        }
    